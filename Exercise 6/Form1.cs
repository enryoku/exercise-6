﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // create instances of separate dice
            Dice oneDie = new Dice();
            Dice twoDie = new Dice();
            // create variables for dice rolls and number of rolls
            int oneDieRoll = 0;
            int twoDieRoll = 0;
            int dieRollCounter = 0;
            // set 2nd die to be the same as the first one
            twoDie.SetSidesOfDie(oneDie.GetSidesOfDie());
            // for loop to keep rolling until we get both of the die roll variables equal to 1
            while (oneDieRoll != 1 && twoDieRoll != 1)
            {
                // roll dice
                oneDieRoll = oneDie.RollDie();
                twoDieRoll = twoDie.RollDie();
                // display roll values
                die1Label.Text = oneDieRoll.ToString();
                die2Label.Text = twoDieRoll.ToString();
                // increment # of rolls
                dieRollCounter++;
            }
            // we only get here if we closed the while loop, display results of our program
            MessageBox.Show("It took " + dieRollCounter + " rolls to get snake eyes with " + oneDie.GetSidesOfDie() + " sided dice!");
        }
    }
}