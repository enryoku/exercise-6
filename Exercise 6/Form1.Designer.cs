﻿namespace Exercise_6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.die1Label = new System.Windows.Forms.Label();
            this.die2Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 117);
            this.button1.TabIndex = 0;
            this.button1.Text = "Roll Dice";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // die1Label
            // 
            this.die1Label.AutoSize = true;
            this.die1Label.BackColor = System.Drawing.Color.Black;
            this.die1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.die1Label.ForeColor = System.Drawing.Color.White;
            this.die1Label.Location = new System.Drawing.Point(149, 34);
            this.die1Label.Name = "die1Label";
            this.die1Label.Padding = new System.Windows.Forms.Padding(7);
            this.die1Label.Size = new System.Drawing.Size(92, 69);
            this.die1Label.TabIndex = 1;
            this.die1Label.Text = "20";
            // 
            // die2Label
            // 
            this.die2Label.AutoSize = true;
            this.die2Label.BackColor = System.Drawing.Color.Black;
            this.die2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.die2Label.ForeColor = System.Drawing.Color.White;
            this.die2Label.Location = new System.Drawing.Point(244, 34);
            this.die2Label.Name = "die2Label";
            this.die2Label.Padding = new System.Windows.Forms.Padding(7);
            this.die2Label.Size = new System.Drawing.Size(92, 69);
            this.die2Label.TabIndex = 2;
            this.die2Label.Text = "20";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.ClientSize = new System.Drawing.Size(341, 141);
            this.Controls.Add(this.die2Label);
            this.Controls.Add(this.die1Label);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label die1Label;
        private System.Windows.Forms.Label die2Label;
    }
}

