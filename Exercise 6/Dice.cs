﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6
{
    class Dice
    {
        // private field for holding number of sides of the die
        private int sidesOfDie;

        // constructor for assigning sides of die
        public Dice()
        {
            Random generateNumber = new Random();
            sidesOfDie = generateNumber.Next(4, 21);
        }

        // method for returning face value of die
        public int RollDie()
        {
            // variable for capturing return value
            int dieRoll = 0;
            // instance and function to create random number between rage established in sidesOfDie
            Random generateNumber = new Random();
            dieRoll = generateNumber.Next(1, (sidesOfDie + 1));
            return dieRoll;
        }

        // method to capture sidesOfDie for making two dice that are the same
        public int GetSidesOfDie()
        {
            return sidesOfDie;
        }

        // method to set sidesOfDie for making two dice that are the same
        public void SetSidesOfDie(int sides)
        {
            sidesOfDie = sides;
        }
    }
}
